﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace BBBanco
{
    class Program
    {
        static void Main(string[] args)
        {
            var brl = new Moeda()
            {
                Nome = "BRL",
                Cotacao = 4.19M
            };
            Conta conta = new Conta();

            var us = new Moeda()
            {
                Nome = "$",
                Cotacao = 1M
            };

            var operacoes = new List<Operacao>();

            conta.Depositar(brl, 50M, operacoes, conta);
            #region Cocastro
            //conta.Depositar(brl, 50M, operacoes, conta);
            //conta.Depositar(brl, 100M, operacoes, conta);
            //conta.Sacar(brl, 100M, operacoes, conta);

            //conta.Depositar(us, 200M, operacoes, conta);
            //conta.Depositar(us, 50M, operacoes, conta);
            //conta.Depositar(us, 50M, operacoes, conta);
            #endregion
            var balance = conta.Saldo(conta, operacoes, brl);
            var balance2 = conta.Saldo(conta, operacoes, us);

            Console.WriteLine(balance.Valor + balance.Moeda.Nome);


            Console.ReadKey();
        }
    }
}
