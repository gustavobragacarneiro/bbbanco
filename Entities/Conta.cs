﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Entities
{
    public class Conta
    {
        public string NomeCliente { get; set; }
        public string EnderecoCliente { get; set; }
        public string TelefoneCliente { get; set; }
        public string EmailCliente { get; set; }
        public int NumeroConta { get; set; }
        public int DigitoConta { get; set; }
        public string Senha { get; set; }


        public void Depositar(Moeda moeda, decimal value, List<Operacao> ops, Conta acc)
        {
            Dinheiro money = new Dinheiro()
            {
                Moeda = moeda,
                Valor = value
            };
            var op = new Operacao()
            {
                Quantia = money.Valor,
                Dinheiro = money,
                Data = DateTime.Now,
                Conta = acc,
                TipoDeOperacao = "Deposito"
            };
            ops.Add(op);
        }
        public void Sacar(Moeda moeda, decimal value, List<Operacao> ops, Conta acc)
        {
            Dinheiro money = new Dinheiro()
            {
                Moeda = moeda,
                Valor = value
            };
            var op = new Operacao()
            {
                Quantia = money.Valor,
                Data = DateTime.Now,
                Conta = acc,
                TipoDeOperacao = "Retirada"
            };
            ops.Add(op);
        }

        public Dinheiro Saldo(Conta acc, List<Operacao> ops, Moeda moeda)
        {
            decimal value = ops.Where(c => c.Conta == acc && c.Dinheiro.Moeda == moeda && c.TipoDeOperacao == "Depositar").
                Aggregate<Operacao, decimal>(0, (current, op) => current + op.Dinheiro.Valor);
            decimal value1 = ops.Where(c => c.Conta == acc && c.Dinheiro.Moeda == moeda && c.TipoDeOperacao == "Retirar").
                Aggregate<Operacao, decimal>(0, (current, op) => current - op.Dinheiro.Valor);
            Dinheiro money = new Dinheiro()
            {
                Moeda = moeda,
                Valor = value + value1
            };
            return money;
        }
        
    }
}