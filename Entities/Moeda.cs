﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Moeda
    {
        public decimal Cotacao { get; set; }

        public string Nome { get; set; }

        public string Simbolo { get; set; }

        //public decimal Converter(decimal valor, Moeda entrada, Moeda saida)
        //{
        //    entrada.Cotacao = Quotes.FirstOrDefault(c => c.Name == "BRL/USD").LastTradePrice;
        //    saida.Cotacao = Quotes.FirstOrDefault(c => c.Name == "BRL/USD").LastTradePrice;

        //    decimal conversor = entrada.Name == "$" ? valor * saida.CotacaoDiaria : valor / saida.CotacaoDiaria;
        //    return conversor;
        //}
    }
}
