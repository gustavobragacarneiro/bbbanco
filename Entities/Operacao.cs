﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Operacao
    {
        public decimal Quantia { get; set; }
        public string TipoDeOperacao { get; set; }
        public DateTime Data { get; set; }
        public string Descricao { get; set; }
        public Moeda Moeda { get; set; }
        public Conta Conta { get; set; }
        public Dinheiro Dinheiro { get; set; }
        public int IdOp { get; set; }

        public void IdOperacao(int IdOp)
        {
            Random random = new Random();
            IdOp = random.Next();
        }

    }
}
